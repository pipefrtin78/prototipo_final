/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronbuilder;

import pojos.Planta;
/**
 *
 * @author pipef
 */
public abstract class ViveroBuilder {
    
    protected Planta plantaB;
    
    public Planta getPlantaPatronBuilder(){
        return plantaB;
    }
    
    public void crearPlantaPatronBuilder(){
        plantaB = new Planta();
    }
    
    public abstract void construirPlanta(int id, String name, String description, int price, int category );
}
