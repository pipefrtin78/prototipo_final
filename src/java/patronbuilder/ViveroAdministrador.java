/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronbuilder;

import pojos.Planta;

/**
 *
 * @author pipef
 */
public class ViveroAdministrador {
    
    private ViveroBuilder viveroBuilder;
    
    public void buildVivero (int id, String name, String description, int price, int category){
        viveroBuilder.crearPlantaPatronBuilder();
        viveroBuilder.construirPlanta(id, name, description, price, category);
    }
    public void setViveroBuilder(ViveroBuilder plantaB){
        viveroBuilder = plantaB;
    }
    
    public Planta getPlantaPatronBuilder(){
        return viveroBuilder.getPlantaPatronBuilder();
    }
    
    
}
