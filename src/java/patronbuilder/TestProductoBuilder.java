

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package patronbuilder;
  
import cruds.CRUDPlantas;
import java.util.LinkedList;
import pojos.Planta;

/**
 *
 * @author pipef
 */
public class TestProductoBuilder {
    
    private static CRUDPlantas crud = new CRUDPlantas();
    
    public static void main(String[] args){
        
        System.out.println("BUILDER -> PLANTA FLORAL >>> ");
        LinkedList<Planta> listaPlantasFloral = crud.listarPlantasBuilder(1);
           for (int x = 0; x < listaPlantasFloral.size(); x++) {
            System.out.println("------------------------------");
            System.out.println("nombre > " + listaPlantasFloral.get(x).getName() + " / precio > "
                    + listaPlantasFloral.get(x).getPrice());
        }
        System.out.println("******************************************************");
        System.out.println("BUILDER -> PLANTA SOMBRA >>> ");
        LinkedList<Planta> listaPlantasSombra = crud.listarPlantasBuilder(2);
        for (int y = 0; y < listaPlantasSombra.size(); y++) {
            System.out.println("------------------------------");
            System.out.println("nombre > " + listaPlantasSombra.get(y).getName() + " / precio > "
                    + listaPlantasSombra.get(y).getPrice());
        }

        System.out.println("******************************************************");
        System.out.println("BUILDER -> PLANTAS SOL >>> ");
        LinkedList<Planta> listaPlantasSol = crud.listarPlantasBuilder(3);
        for (int z = 0; z < listaPlantasSol.size(); z++) {
            System.out.println("------------------------------");
            System.out.println("nombre > " + listaPlantasSol.get(z).getName() + " / precio > "
                    + listaPlantasSol.get(z).getPrice());
        }

        System.out.println("******************************************************");
        System.out.println("BUILDER -> PLANTAS ARBOLES >>> ");
        LinkedList<Planta> listaPlantasArboles = crud.listarPlantasBuilder(4);
        for (int c = 0; c < listaPlantasArboles.size(); c++) {
            System.out.println("------------------------------");
            System.out.println("nombre > " + listaPlantasArboles.get(c).getName() + " / precio > "
                    + listaPlantasArboles.get(c).getPrice());
        }
        System.out.println("******************************************************");
        
        System.out.println("******************************************************");
        System.out.println("BUILDER -> PLANTAS CACTUS>>> ");
        LinkedList<Planta> listaPlantasCactus = crud.listarPlantasBuilder(5);
        for (int c = 0; c < listaPlantasCactus.size(); c++) {
            System.out.println("------------------------------");
            System.out.println("nombre > " + listaPlantasCactus.get(c).getName() + " / precio > "
                    + listaPlantasCactus.get(c).getPrice());
        }
        System.out.println("******************************************************");
    }
    

}


