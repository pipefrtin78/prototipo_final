/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connector;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author pipef
 */
public class ConexionObjetos {
   
    private BeanConnector bConn;
   //AUTORESFERENCIA EL OBJETO QUE SE VA A INSTANCIAR DE MANERA UNICA
     private static ConexionObjetos cxObjects;
     
     //CAMBIO EL MODIFICADOR DE ACCESO DEL CONSTRUCTOR DE PUBLIC -> PRIVATE ->
     
    public ConexionObjetos(){
        try{
            bConn = new BeanConnector();
            bConn.setDriver("com.mysql.cj.jdbc.Driver");
            bConn.setUser("root");
            bConn.setPassword("root");
            bConn.setUrl("jdbc:mysql://localhost/vivero?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
           
            bConn.conectar();
        }catch(Exception e){
            e.printStackTrace();
        }
       
    }
    
    public static ConexionObjetos getInstance(){
        
        if(cxObjects == null){
            cxObjects = new ConexionObjetos();
        }else{
            System.out.println("ERROR, ESTA TRATANDO DE INSTANCIAR UN OBJETO SINGLETON QUE YA EXISTE");
        
        }
        return cxObjects;
                
    }
    
    //SOBREESCRIBO EL METODO CLONE Y RESTRINGO LA CLONACION DE OBJETOS 
    
        @Override
        public ConexionObjetos clone(){
        
            try{
                throw new CloneNotSupportedException();
    
          }catch (CloneNotSupportedException e){
          
                System.out.println("ESTE OBJETO NO SE PUEDE CLONAR, SOY UNICO - SINGLETON");
          }
            return null;
    
    }
   
    public BeanConnector getbConn(){
            return bConn;
    }

 

//    public static class getInstance extends ConexionObjetos {

 

    //    public getInstance() {
   //     }
   // }
   
    
}