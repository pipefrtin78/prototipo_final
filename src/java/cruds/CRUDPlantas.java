/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cruds;

import connector.ConexionObjetos;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import patronbuilder.BuilderPlantaArbol;
import patronbuilder.BuilderPlantaCactus;
import patronbuilder.BuilderPlantaFloral;
import patronbuilder.BuilderPlantaSol;
import patronbuilder.BuilderPlantaSombra;
import patronbuilder.ViveroAdministrador;
import pojos.Planta;
/**
 *
 * @author pipef
 */
public class CRUDPlantas {
     private static ConexionObjetos conn;
    
    public CRUDPlantas(){
        try{
            conn = ConexionObjetos.getInstance();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static LinkedList<Planta> listarPlantas(){
        LinkedList<Planta> listaPlantas = new LinkedList<>();
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM planta, category WHERE planta.category = category.id");
            while(rs.next()){
                Planta planta = new Planta();
                planta.setId(rs.getInt("id"));
                planta.setName(rs.getString("name"));
                planta.setDescription(rs.getString("description"));
                planta.setPrice(rs.getInt("price"));
                planta.setId_category(rs.getInt("category"));
                
                planta.setCategoryName(rs.getString("category.name"));
                
                listaPlantas.add(planta);
            }
            rs.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return listaPlantas;
    }
    public static LinkedList<Planta> listarPlantasBuilder(int categoryB) {
        LinkedList<Planta> listaPlantasFloral = new LinkedList<>();
        LinkedList<Planta> listaPlantasSombra = new LinkedList<>();
        LinkedList<Planta> listaPlantasSol = new LinkedList<>();
        LinkedList<Planta> listaPlantasArboles = new LinkedList<>();
        LinkedList<Planta> listaPlantasCactus = new LinkedList<>();

        ViveroAdministrador va = new ViveroAdministrador();

        try {
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM planta "
                    + " WHERE category = " + categoryB);

            while (rs.next()) {

                if (categoryB == 1) {
                    va.setViveroBuilder(new BuilderPlantaFloral());
                    va.buildVivero(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlantasFloral.add(va.getPlantaPatronBuilder());

                } else if (categoryB == 4) {
                    va.setViveroBuilder(new BuilderPlantaArbol());
                    va.buildVivero(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlantasArboles.add(va.getPlantaPatronBuilder());

                } else if (categoryB == 2) {
                    va.setViveroBuilder(new BuilderPlantaSombra());
                    va.buildVivero(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlantasSombra.add(va.getPlantaPatronBuilder());

                } else if (categoryB == 3) {
                    va.setViveroBuilder(new BuilderPlantaSol());
                    va.buildVivero(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlantasSol.add(va.getPlantaPatronBuilder());
                    
                }else if (categoryB == 5) {
                    va.setViveroBuilder(new BuilderPlantaCactus());
                    va.buildVivero(rs.getInt("id"),rs.getString("name"), rs.getString("description"),
                            rs.getInt("price"), categoryB);
                    listaPlantasCactus.add(va.getPlantaPatronBuilder());
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (categoryB == 1) {
            return listaPlantasFloral;
        } else if (categoryB == 4) {
            return listaPlantasArboles;
        } else if (categoryB == 2) {
            return listaPlantasSombra;
        } else if (categoryB == 3) {
            return listaPlantasSol;
        } else if (categoryB == 5) {
            return listaPlantasCactus;
        }
        return null;
    }
    
    public void insertarPlanta(Planta planta){
        //lanta nuevaPlanta = new Planta();
        String name =planta.getName();
        String description = planta.getDescription();
        int price = planta.getPrice();
        int category = planta.getId_category();
        
        try{
            conn.getbConn().getSt().executeUpdate("INSERT INTO planta (name, description, price, category)"
                    + "values ('" + name + "', '"+description+" ', '"+price+"', '"+category+"')");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void modificarPlanta(Planta planta){
        int id = planta.getId();
        String name = planta.getName();
        String description = planta.getDescription();
        int price = planta.getPrice();
        int category = planta.getId_category();
        
        try{
            conn.getbConn().getSt().executeUpdate("UPDATE planta SET name='"+name+"',"
                    +"description= '"+description
                    +"', price = '"+price
                    +"', category = '"+category
                    +"' WHERE id = "+ id);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void borrarPlanta (int id){
    try{
        conn.getbConn().getSt().executeUpdate("DELETE FROM planta WHERE id = " + id);
    } catch (Exception e){
        e.printStackTrace();
    }
   
   }
 
            
}
