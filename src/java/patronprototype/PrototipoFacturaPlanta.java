/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author pipef
 */
public abstract class PrototipoFacturaPlanta implements Cloneable{
    private String utilidad;
    private int tiempoVida;
    private int serialFactura;
    
    public PrototipoFacturaPlanta(String utilidad, int tiempoVida, int serialFactura){
        this.utilidad = utilidad; 
        this.tiempoVida = tiempoVida;
        this.serialFactura = serialFactura;
    }
    
    
 
    public Object clone()throws CloneNotSupportedException{
        
        return super.clone();
    }

    public String getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(String utilidad) {
        this.utilidad = utilidad;
    }

    public int getTiempoVida() {
        return tiempoVida;
    }

    public void setTiempoVida(int tiempoVida) {
        this.tiempoVida = tiempoVida;
    }

    public int getSerialFactura() {
        return serialFactura;
    }

    public void setSerialFactura(int serialFactura) {
        this.serialFactura = serialFactura;
    }



   
}
