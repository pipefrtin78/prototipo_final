/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author pipef
 */
    public class PlantaBolsa extends PrototipoFacturaPlanta{
    private int precioBolsa;
    
    public PlantaBolsa (String utilidad, int tiempoVida, int precioBolsa, int precio){
        super (utilidad, tiempoVida, precio);
        this.precioBolsa = precioBolsa;
    }

    public int getPrecioBolsa() {
        return precioBolsa;
    }

    public void setPrecioBolsa(int precioBolsa) {
        this.precioBolsa = precioBolsa;
    }
    
}
