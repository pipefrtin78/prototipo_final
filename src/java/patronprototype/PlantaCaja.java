/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author pipef
 */
public class PlantaCaja extends PrototipoFacturaPlanta{
    private int precioCaja;
    
    public PlantaCaja (String utilidad, int tiempoVida, int precioCaja, int precio){
        super (utilidad, tiempoVida, precio);
        this.precioCaja = precioCaja;
        
    }

    public int getPrecioCaja() {
        return precioCaja;
    }

    public void setPrecioCaja(int precioCaja) {
        this.precioCaja = precioCaja;
    }
}
