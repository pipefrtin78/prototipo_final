/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

import java.util.ArrayList;

/**
 *
 * @author pipef
 */
public class ClientePrototypePlanta {
    public static void main(String args[])throws CloneNotSupportedException{
        int capacidadCantidadPlantas = 10;
        PrototipoFacturaPlanta prototipoPlantaCaja = new PlantaCaja("Adorno", 15, 4, 1);
        PrototipoFacturaPlanta prototipoPlantaMatera = new PlantaMatera("Fruto", 25, 7, 2);
        PrototipoFacturaPlanta prototipoPlantaBolsa = new PlantaBolsa("Funcion", 70, 9, 3);
         ArrayList listaFacturaPlantas = new ArrayList();
        for(int i=0; i<capacidadCantidadPlantas; i++){
           PrototipoFacturaPlanta plantacaja = (PrototipoFacturaPlanta) prototipoPlantaCaja.clone();
           plantacaja.setSerialFactura(i*3);
           System.out.println("Utilidad: "+plantacaja.getUtilidad()+ " / " +"Tiempo de Vida: "+ plantacaja.getTiempoVida()+ " / "+
                    "Serial: "+plantacaja.getSerialFactura());
           PrototipoFacturaPlanta plantamatera = (PrototipoFacturaPlanta) prototipoPlantaMatera.clone();
           plantamatera.setSerialFactura(i*4);
            System.out.println("Utilidad: "+plantamatera.getUtilidad()+ " / " +"Tiempo de Vida: "+ plantamatera.getTiempoVida()+ " / "+
                    "Serial: "+plantamatera.getSerialFactura());
             PrototipoFacturaPlanta plantabolsa = (PrototipoFacturaPlanta) prototipoPlantaBolsa.clone();
           plantabolsa.setSerialFactura(i*5);
            System.out.println("Utilidad: "+plantabolsa.getUtilidad()+ " / " +"Tiempo de Vida: "+ plantabolsa.getTiempoVida()+ " / "+
                    "Serial: "+plantabolsa.getSerialFactura());
            
        }
    }
    
}
