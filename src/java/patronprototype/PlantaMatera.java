/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronprototype;

/**
 *
 * @author pipef
 */
    public class PlantaMatera extends PrototipoFacturaPlanta{
    private int precioMatera;
    
    public PlantaMatera (String utilidad, int tiempoVida,  int precioMatera, int precio){
        super (utilidad, tiempoVida, precio);
        this.precioMatera = precioMatera;
    }

    public int getPrecioMatera() {
        return precioMatera;
    }

    public void setPrecioMatera(int precioMatera) {
        this.precioMatera = precioMatera;
    }
    
}
