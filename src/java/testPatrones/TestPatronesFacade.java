/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testPatrones;

import connector.ConexionObjetos;
import cruds.CRUDPlantas;
import java.sql.ResultSet;
import java.util.LinkedList;
import patronfacade.FacadeVivero;
import patronfacade.ViveroFacade;
import pojos.Planta;
/**
 *
 * @author pipef
 */
public class TestPatronesFacade {
     static FacadeVivero facadeVivero;
    
    public static void main(String args[]){
        facadeVivero = new FacadeVivero();
        listarPlantas();
     
       }
        public static void listarPlantas(){
        LinkedList <ViveroFacade> listaPlantasFacade = facadeVivero.generarPlantasFacade();
            System.out.println("__________________________________________________________________________________________");
        for(int x = 0; x < listaPlantasFacade.size(); x++){
        System.out.println("name --> "+listaPlantasFacade.get(x).getNamePlanta()+ "/ price $ >>>> "+ 
                listaPlantasFacade.get(x).getPricePlanta());
        ViveroFacade planta = listaPlantasFacade.get(x);
        System.out.println("description --> "+planta.getDescriptionPlanta()+ "/ category  >>>> "+ planta.getCategoryPlanta());
        }
        
       }
       
         
}

