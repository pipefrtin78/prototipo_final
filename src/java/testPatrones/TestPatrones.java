/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testPatrones;

import connector.ConexionObjetos;
import cruds.CRUDPlantas;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Planta;
/**
 *
 * @author pipef
 */
public class TestPatrones {
  static CRUDPlantas crudPlantas = new CRUDPlantas();
    
    public static void main(String args[]){
       /*
        try{
            
           
        ConexionObjetos co = new ConexionObjetos.getInstance();
        ResultSet rs = co.getbConn().getSt().executeQuery("SELECT * FROM pelicula");
        while(rs.next()){
            System.out.println("name -->  " + rs.getString("name"));
        }
        rs.close();
        
        ConexionObjetos co2 = co.clone();
        
     }catch(Exception e){
       e.printStackTrace();
     }
*/
      crudPlantas = new CRUDPlantas();
      listarPlantas();
     
     crudPlantas = new CRUDPlantas();
     Planta plantaNueva = new Planta();
     plantaNueva.setName("The Godfather parte 3 nueva versión");
     plantaNueva.setDescription("Escenas ineditas de la tercera parte, nunca antes vistas");
     plantaNueva.setPrice(20000);
     plantaNueva.setId_category(3); 
     crudPlantas.insertarPlanta(plantaNueva);
     listarPlantas();   
     System.out.println("NUEVA PLANTA");

  
     Planta plantaActualizada = new Planta();
     plantaActualizada.setId(25);
     plantaActualizada.setName("bateria");
     plantaActualizada.setDescription("conjunto de instrumentos de tipo percusion");
     plantaActualizada.setPrice(500000);
     plantaActualizada.setId_category(2);
     crudPlantas.modificarPlanta(plantaActualizada);
     
     listarPlantas();
     System.out.println("PLANTA ACTUALIZADA");
   
     crudPlantas.borrarPlanta(20);
     listarPlantas();
        System.out.println("PLANTA ELIMINADA");
   
    }
    public static void listarPlantas(){
    LinkedList<Planta> listaPlantas = crudPlantas.listarPlantas();
    System.out.println("--------------------------------------------------------------------------------------------------");
    for(int x=0 ;x< listaPlantas.size(); x++){
        System.out.println("name ----> "+listaPlantas.get(x).getName() + " / price $ >>> "+listaPlantas.get(x).getPrice());
        Planta planta = listaPlantas.get(x);
        System.out.println("description ---> " + planta.getDescription()+" / category >>> " +planta.getCategoryName());
    }
  }
}

