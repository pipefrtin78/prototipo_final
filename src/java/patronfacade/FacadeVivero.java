/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfacade;
import connector.ConexionObjetos;
import cruds.CRUDPlantas;
import java.sql.ResultSet;
import java.util.LinkedList;
import pojos.Category;
import pojos.Planta;
import patronfacade.ViveroFacade;
/**
 *
 * @author pipef
 */
public class FacadeVivero {
    private CRUDPlantas crudPlantas;
    private ConexionObjetos conn;
    
    
    public FacadeVivero(){
        crudPlantas = new CRUDPlantas();
        conn = new ConexionObjetos();
    }
    
    public LinkedList<ViveroFacade> generarPlantasFacade(){
        LinkedList<ViveroFacade> ListaPlantasCategoria = new LinkedList();
        LinkedList<Planta> listaPlantas = crudPlantas.listarPlantas();
        LinkedList<Category> listaCategorias = new LinkedList<>();
        
        try{
            ResultSet rs = conn.getbConn().getSt().executeQuery("SELECT * FROM category");
            
            while(rs.next()){
                Category cat = new Category();
                cat.setId(rs.getInt("id"));
                cat.setName(rs.getString("name"));
                listaCategorias.add(cat);
                
            }
            rs.close();
            
        }catch (Exception e){
            e.printStackTrace();
        }
        
        for(int x=0; x < listaPlantas.size(); x ++){
            for(int y = 0; y < listaCategorias.size(); y++){
                if(listaPlantas.get(x).getId_category() == listaCategorias.get(y).getId()){
                    listaPlantas.get(x).setCategoryName(listaCategorias.get(y).getName());
                    System.out.println("id_categoria >>> "+ listaPlantas.get(x).getId_category());
                    System.out.println("name_categoria >>>"+listaCategorias.get(y).getName());
                    System.out.println("_______________________________________________________________");
                    
                }
            }
            Planta p = listaPlantas.get(x);
            ViveroFacade pf = new ViveroFacade(p.getId(), p.getName(), p.getDescription(), p.getPrice(), p.getCategoryName());
            ListaPlantasCategoria.add(pf);
        }
        
        return ListaPlantasCategoria;
    }
    
}