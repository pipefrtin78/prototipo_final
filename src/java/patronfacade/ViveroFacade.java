/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronfacade;

/**
 *
 * @author pipef
 */
public class ViveroFacade {
     private int id;
    private String namePlanta;
    private String descriptionPlanta;
    private int pricePlanta;
    private String categoryPlanta;
    
    public ViveroFacade(int id, String namePlanta, String descriptionPlanta, int pricePlanta, String categoryPlanta){
        this.id = id;
        this.namePlanta = namePlanta;
        this.descriptionPlanta = descriptionPlanta;
        this.pricePlanta = pricePlanta;
        this.categoryPlanta = categoryPlanta;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamePlanta() {
        return namePlanta;
    }

    public void setNamePlanta(String namePlanta) {
        this.namePlanta = namePlanta;
    }

    public String getDescriptionPlanta() {
        return descriptionPlanta;
    }

    public void setDescriptionPlanta(String descriptionPlanta) {
        this.descriptionPlanta = descriptionPlanta;
    }

    public int getPricePlanta() {
        return pricePlanta;
    }

    public void setPricePlanta(int pricePlanta) {
        this.pricePlanta = pricePlanta;
    }

    public String getCategoryPlanta() {
        return categoryPlanta;
    }

    public void setCategoryPlanta(String categoryPlanta) {
        this.categoryPlanta = categoryPlanta;
    }
    
}
