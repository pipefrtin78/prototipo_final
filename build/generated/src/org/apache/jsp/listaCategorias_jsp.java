package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import patronfacade.ViveroFacade;
import patronfacade.FacadeVivero;
import pojos.Category;
import cruds.CRUDCategorias;
import pojos.Planta;
import java.util.LinkedList;
import cruds.CRUDPlantas;

public final class listaCategorias_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <center><title>Lista Categorias</title></center>\n");
      out.write("    </head>\n");
      out.write("     <body style=\"background-image: url('fondo.jpg'); background-size: 100%\">\n");
      out.write("        <h1>Lista Categorias Vivero </h1>\n");
      out.write("        <br>\n");
      out.write("        <center><colgroup><table border=\"1\" style=\"width:95%\" text-align=\"center;\" class=\"egt\"></center>\n");
      out.write("            <tr>\n");
      out.write("                <center><fieldset style=\"width:1100px\"><legend><h1 style=\"color:black;\">LISTA VIVERO CATEGORIA FLORALES >>> PATRON BUILDER</h1></legend>\n");
      out.write("                <tr align=\"center\">\n");
      out.write("                <colgroup span=\"1\" style=\"background: honeydew\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: moccasin\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: palegreen\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: paleturquoise\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: mediumaquamarine\"></colgroup>\n");
      out.write("                <th> IDENTIFICATIVO </th>\n");
      out.write("                <th> NOMBRE </th>\n");
      out.write("                <th> DESCRIPCIÓN </th>\n");
      out.write("                <th> PRECIO </th>\n");
      out.write("                <th> CATEGORÍA </th>\n");
      out.write("            \n");
      out.write("                        ");

                            CRUDPlantas crudPlantas = new CRUDPlantas();
                            LinkedList<Planta> listaPlantasFlorales = crudPlantas.listarPlantasBuilder(1);
                            for (int i = 0; i < listaPlantasFlorales.size(); i++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getId() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getName() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getPrice() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                        </tr>\n");
      out.write("                </table> </center> \n");
      out.write("                    </fieldset>\n");
      out.write("            <tr>\n");
      out.write("                <td> LISTA VIVERO CATEGORIA SOMBRA >>> PATRON BUILDER\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>    \n");
      out.write("                            <td>codigo Planta</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Planta> listaPlantasSombra = crudPlantas.listarPlantasBuilder(2);
                            for (int x = 0; x < listaPlantasSombra.size(); x++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlantasSombra.get(x).getId() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getName() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getPrice() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>\n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td>LISTA VIVERO CATEGORIA SOL >>> PATRON BUILDER\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo Planta</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Planta> listaPlantasSol = crudPlantas.listarPlantasBuilder(3);
                            for (int y = 0; y < listaPlantasSol.size(); y++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlantasSol.get(y).getId() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getName() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getPrice() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>   \n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td>LISTA VIVERO CATEGORIA ARBOLES >>> PATRON BUILDER\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo Plantas</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Planta> listaPlantasArboles = crudPlantas.listarPlantasBuilder(4);
                            for (int z = 0; z < listaPlantasArboles.size(); z++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlantasArboles.get(z).getId() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getName() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getPrice() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>   \n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("            <td>LISTA VIVERO CATEGORIA CACTUS >>> PATRON BUILDER\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo Plantas</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            LinkedList<Planta> listaPlantasCactus = crudPlantas.listarPlantasBuilder(5);
                            for (int z = 0; z < listaPlantasArboles.size(); z++) {
                                out.println("<tr>");
                                out.println("<td>" + listaPlantasCactus.get(z).getId() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getName() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getPrice() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        
      out.write("\n");
      out.write("                    </table>   \n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <td>LISTA JOYERIAS >>> PATRON FACADE\n");
      out.write("                    <table border =\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>codigo Planta</td>\n");
      out.write("                            <td>nombre</td>\n");
      out.write("                            <td>descripcion</td> \n");
      out.write("                            <td>precio</td>\n");
      out.write("                            <td>categoria</td>\n");
      out.write("                            <td>id_categoria</td>\n");
      out.write("                            <td>nombre_categoria</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            CRUDCategorias crudCategorias = new CRUDCategorias();
                            LinkedList<Planta> listaPlantas = crudPlantas.listarPlantas();
                            LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();

                            for (int x = 0; x < listaPlantas.size(); x++) {
                                for (int y = 0; y < listaCategorias.size(); y++) {
                                    if (listaPlantas.get(x).getId_category()== listaCategorias.get(y).getId()) {
                                        listaPlantas.get(x).setCategoryName(listaCategorias.get(y).getName());
                                        Planta planta = listaPlantas.get(x);
                                        Category categoria = listaCategorias.get(y);
                                        out.println("<tr>");
                                        out.println("<td>" + planta.getId() + "</td>");
                                        out.println("<td>" + planta.getName() + "</td>");
                                        out.println("<td>" + planta.getDescription() + "</td>");
                                        out.println("<td>" + planta.getPrice() + "</td>");
                                        out.println("<td>" + planta.getId_category()+ "</td>");
                                        out.println("<td>" + categoria.getId() + "</td>");
                                        out.println("<td>" + categoria.getName() + "</td>");
                                        out.println("</tr>");
                                    }
                                }
                            }


                        
      out.write("\n");
      out.write("                    </table>   \n");
      out.write("                </td>\n");
      out.write("            </tr>\n");
      out.write("        </table>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"plantas.jsp\">Volver al inicio</a>\n");
      out.write("        <hr>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
