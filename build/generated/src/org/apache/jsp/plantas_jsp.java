package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Iterator;
import pojos.Category;
import patroniterator.IteratorListaCategorias;
import cruds.CRUDCategorias;
import pojos.Planta;
import java.util.LinkedList;
import cruds.CRUDPlantas;

public final class plantas_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Sistema Vivero </title>\n");
      out.write("         </head>\n");
      out.write("         <body style=\"background-image: url('fondo.jpg'); background-size: 100%\">\n");
      out.write("        <div align=\"center\"><img src=\"lluvia.gif\" alt=\"Img Vivero\" style=\"width:1200px;height:200px;border: 10px solid; color: azure\"></div>\n");
      out.write("        <center><fieldset style=\"width:1100px\"><legend><h1 style=\"color:black;\">Vivero Inventario</h1></legend>\n");
      out.write("        <center><colgroup><table border=\"1\" style=\"width:95%\" text-align=\"center;\" class=\"egt\"><center>\n");
      out.write("                <tr align=\"center\">\n");
      out.write("                <colgroup span=\"1\" style=\"background: honeydew\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: moccasin\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: palegreen\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: paleturquoise\"></colgroup>\n");
      out.write("                <colgroup span=\"1\" style=\"background: mediumaquamarine\"></colgroup>\n");
      out.write("                <th> IDENTIFICATIVO </th>\n");
      out.write("                <th> NOMBRE </th>\n");
      out.write("                <th> DESCRIPCIÓN </th>\n");
      out.write("                <th> PRECIO </th>\n");
      out.write("                <th> CATEGORÍA </th>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                ");

                    CRUDPlantas crudPeliculas = new CRUDPlantas();
                    LinkedList<Planta> lista = crudPeliculas.listarPlantas();
                    for(int i = 0; i < lista.size() ; i++){
                    Planta planta = lista.get(i);
                    out.println("<tr align=center>");
                    out.println("<td>"+ planta.getId()+ "</td>");
                    out.println("<td>"+ planta.getName()+ "</td>");
                    out.println("<td>"+ planta.getDescription()+ "</td>");
                    out.println("<td>"+ planta.getPrice()+ "</td>");
                    out.println("<td>"+ planta.getId_category()+ "</td>");
                    out.println("</tr>");
                }
                
      out.write("\n");
      out.write("                </tr>\n");
      out.write("                </colorgroup>\n");
      out.write("            </table> </center> \n");
      out.write("                </fieldset> </center>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("        <a href=\"listaCategorias.jsp\" ><center><h2>Listas de Plantas por Categoria </h2></center><a/>\n");
      out.write("        <form name=\"formularioPlantas\" action=\"ControladorVivero\"method=\"get\"><center>\n");
      out.write("            <fieldset style=\"width:500px\">\n");
      out.write("            <legend><h2 style=\"color:black;\"><center>Funciones</center></h2> </legend>\n");
      out.write("            <p style=\"text-align:left;\"> \n");
      out.write("            CÓDIGO PLANTA:      <input type=\"text\" name=\"id\" size=\"10\" placeholder=\"Ingrese Id\" align='left' > <br>\n");
      out.write("            NOMBRE PLANTA:      <input type=\"text\" name=\"name\" size=\"25\"placeholder=\"Ingrese Nombre\"><br>\n");
      out.write("            DESCRIPCIÓN PLANTA: <input type=\"text\" name=\"description\" size=\"25\"placeholder=\"Ingrese Descripcion Planta...\"><br>\n");
      out.write("            PRECIO PLANTA:      <input type=\"text\" name=\"price\" size=\"10\"placeholder=\"Ingrese Precio\"><br>\n");
      out.write("            PATRON ITERATOR >>> <br> \n");
      out.write("            CATEGORIA > \n");
      out.write("            <select name=\"category\">\n");
      out.write("                ");

                 CRUDCategorias crudCategorias = new CRUDCategorias();
                 IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                 LinkedList<Category> listCat = crudCategorias.listarCategorias();
                 
                 for (int x = 0; x < listCat.size();x++){
                 listaCategorias.add(listCat.get(x).getId());
                 }
                 
                 Iterator<Category> iteradorCategorias = listaCategorias.iterator();
                 while(iteradorCategorias.hasNext()){
                     Category categoria = (Category)iteradorCategorias.next();
                     out.println("<option value = \""+categoria.getId()+ "\">" + categoria.getName()+ "</option>");
                 }
                 
      out.write("\n");
      out.write("            </select>\n");
      out.write("            </p>\n");
      out.write("            </fieldset>\n");
      out.write("            <hr>\n");
      out.write("            <br>\n");
      out.write("            <input type=\"submit\" name=\"insertarplanta\" value=\"AÑADIR PLANTA\" style=\"color: green; border: 3px solid; width: 150px; height: 30px\">\n");
      out.write("            <input type=\"submit\" name=\"modificarplanta\" value=\"ACTUALIZAR PLANTA\"style=\"color: blue; border: 3px solid; width: 170px; height: 30px\">\n");
      out.write("            <input type=\"submit\" name=\"borrarplanta\" value=\"BORRAR PLANTA\" style=\"color: red; border: 3px solid; width: 160px; height: 30px\">\n");
      out.write("            </center>\n");
      out.write("        </form>\n");
      out.write("        <br>\n");
      out.write("        <hr>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
