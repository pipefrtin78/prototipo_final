<%-- 
    Document   : listaCategorias
    Created on : 25/11/2020, 07:44:29 PM
    Author     : pipef
--%>

<%@page import="patronfacade.ViveroFacade"%>
<%@page import="patronfacade.FacadeVivero"%>
<%@page import="pojos.Category"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="pojos.Planta"%>
<%@page import="java.util.LinkedList"%>
<%@page import="cruds.CRUDPlantas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <center><title>Lista Categorias</title></center>
    </head>
     <body style="background-image: url('fondo.jpg'); background-size: 100%">
     <center><h1>Lista Categorias Vivero </h1></center>
        <br>
        <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA VIVERO CATEGORIA FLORALES >>> PATRON BUILDER</h1></legend>
                <tr align="center">
        <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
            <tr>
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
                
                </tr>
                        <%
                            CRUDPlantas crudPlantas = new CRUDPlantas();
                            LinkedList<Planta> listaPlantasFlorales = crudPlantas.listarPlantasBuilder(1);
                            for (int i = 0; i < listaPlantasFlorales.size(); i++) {
                                out.println("<tr align=center>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getId()+ "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getName() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasFlorales.get(i).getPrice() + "</td>");
                               // out.println("<td>" + listaPlantasFlorales.get(i).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                
                </table> </center> 
                    </fieldset>
            <tr>
                <br>
                <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA VIVERO CATEGORIA SOMBRA >>> PATRON BUILDER</h1></legend>
                <tr align="center">
                
                  <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
                  <tr align="center">
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
               
                </tr>
                        <%
                            LinkedList<Planta> listaPlantasSombra = crudPlantas.listarPlantasBuilder(2);
                            for (int x = 0; x < listaPlantasSombra.size(); x++) {
                                out.println("<tr align=center>");
                                out.println("<td>" + listaPlantasSombra.get(x).getId() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getName() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasSombra.get(x).getPrice() + "</td>");
                                //out.println("<td>" + listaPlantasSombra.get(x).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>
                </fieldset>
                </td>
            </tr>
            <tr>
                <br>
                <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA VIVERO CATEGORIA SOL >>> PATRON BUILDER</h1></legend>
                    <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
                  <tr align="center">
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
                
                </tr>
                        <%
                            LinkedList<Planta> listaPlantasSol = crudPlantas.listarPlantasBuilder(3);
                            for (int y = 0; y < listaPlantasSol.size(); y++) {
                                out.println("<tr align=center>");
                                out.println("<td>" + listaPlantasSol.get(y).getId() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getName() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasSol.get(y).getPrice() + "</td>");
                               // out.println("<td>" + listaPlantasSol.get(y).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>  
                    </fieldset>
                </td>
            </tr>
            <tr>
                <br>
                <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA VIVERO CATEGORIA ARBOLES >>> PATRON BUILDER</h1></legend>
               
                    <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
                  <tr align="center">
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
                
                </tr>
                        <%
                            LinkedList<Planta> listaPlantasArboles = crudPlantas.listarPlantasBuilder(4);
                            for (int z = 0; z < listaPlantasArboles.size(); z++) {
                                out.println("<tr align=center>");
                                out.println("<td>" + listaPlantasArboles.get(z).getId() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getName() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasArboles.get(z).getPrice() + "</td>");
                               // out.println("<td>" + listaPlantasArboles.get(z).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>   
                    </fieldset>
                </td>
            </tr>
            <tr>
                <br>
                <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA VIVERO CATEGORIA CACTUS >>> PATRON BUILDER</h1></legend>
                    <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
                  <tr align="center">
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
               
                </tr>
                        <%
                            LinkedList<Planta> listaPlantasCactus = crudPlantas.listarPlantasBuilder(5);
                            for (int z = 0; z < listaPlantasCactus.size(); z++) {
                                out.println("<tr align=center>");
                                out.println("<td>" + listaPlantasCactus.get(z).getId() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getName() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getDescription() + "</td>");
                                out.println("<td>" + listaPlantasCactus.get(z).getPrice() + "</td>");
                               // out.println("<td>" + listaPlantasCactus.get(z).getId_category()+ "</td>");
                                out.println("</tr>");
                            }
                        %>
                    </table>   
                    </fieldset>
                </td>
            </tr>
            <tr>
                <br>
                <center><fieldset style="width:1100px"><legend><h1 style="color:black;">LISTA PLANTAS >>> PATRON FACADE</h1></legend>
                    <center><colgroup><table border="1" style="width:70%" text-align="center;" class="egt"></center>
                        <tr>
                            <colgroup span="1" style="background: honeydew"></colgroup>
                            <colgroup span="1" style="background: moccasin"></colgroup>
                            <colgroup span="1" style="background: palegreen"></colgroup>
                            <colgroup span="1" style="background: paleturquoise"></colgroup>
                            <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                            <colgroup span="1" style="background: yellow"></colgroup>
                            <td>IDENTIFICATIVO</td>
                            <td>NOMBRE</td>
                            <td>DESCRIPCIÓN</td> 
                            <td>PRECIO</td>
                            
                            <td>NOMBRE CATEGORIA</td>
                        </tr>
                        <%
                            CRUDCategorias crudCategorias = new CRUDCategorias();
                            LinkedList<Planta> listaPlantas = crudPlantas.listarPlantas();
                            LinkedList<Category> listaCategorias = crudCategorias.listarCategorias();

                            for (int x = 0; x < listaPlantas.size(); x++) {
                                for (int y = 0; y < listaCategorias.size(); y++) {
                                    if (listaPlantas.get(x).getId_category()== listaCategorias.get(y).getId()) {
                                        listaPlantas.get(x).setCategoryName(listaCategorias.get(y).getName());
                                        Planta planta = listaPlantas.get(x);
                                        Category categoria = listaCategorias.get(y);
                                        out.println("<tr align=center>");
                                        out.println("<td>" + planta.getId() + "</td>");
                                        out.println("<td>" + planta.getName() + "</td>");
                                        out.println("<td>" + planta.getDescription() + "</td>");
                                        out.println("<td>" + planta.getPrice() + "</td>");
                                        //out.println("<td>" + planta.getId_category()+ "</td>");
                                        out.println("<td>" + categoria.getName() + "</td>");
                                        out.println("</tr>");
                                    }
                                }
                            }


                        %>
                    </table>  
                    </fieldset>
                </td>
            </tr>
        </table>
        <br>
        <hr>
        <a href="plantas.jsp" ><center><h2 style="color: red">Volver a Gestion de Plantas Vivero </h2></center><a/>
        <hr>
    </body>
</html>

