<%-- 
    Document   : plantas
    Created on : 4/11/2020, 06:46:09 PM
    Author     : pipef
--%>

<%@page import="java.util.Iterator"%>
<%@page import="pojos.Category"%>
<%@page import="patroniterator.IteratorListaCategorias"%>
<%@page import="cruds.CRUDCategorias"%>
<%@page import="pojos.Planta"%>
<%@page import="java.util.LinkedList"%>
<%@page import="cruds.CRUDPlantas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema Vivero </title>
         </head>
         <body style="background-image: url('fondo.jpg'); background-size: 100%">
        <div align="center"><img src="lluvia.gif" alt="Img Vivero" style="width:1200px;height:200px;border: 10px solid; color: azure"></div>
        <center><fieldset style="width:1100px"><legend><h1 style="color:black;">Vivero Inventario</h1></legend>
        <center><colgroup><table border="1" style="width:95%" text-align="center;" class="egt"><center>
                <tr align="center">
                <colgroup span="1" style="background: honeydew"></colgroup>
                <colgroup span="1" style="background: moccasin"></colgroup>
                <colgroup span="1" style="background: palegreen"></colgroup>
                <colgroup span="1" style="background: paleturquoise"></colgroup>
                <colgroup span="1" style="background: mediumaquamarine"></colgroup>
                <th> IDENTIFICATIVO </th>
                <th> NOMBRE </th>
                <th> DESCRIPCIÓN </th>
                <th> PRECIO </th>
                <th> CATEGORÍA </th>
            </tr>
            <tr>
                <%
                    CRUDPlantas crudPlantas = new CRUDPlantas();
                    LinkedList<Planta> lista = crudPlantas.listarPlantas();
                    for(int i = 0; i < lista.size() ; i++){
                    Planta planta = lista.get(i);
                    out.println("<tr align=center>");
                    out.println("<td>"+ planta.getId()+ "</td>");
                    out.println("<td>"+ planta.getName()+ "</td>");
                    out.println("<td>"+ planta.getDescription()+ "</td>");
                    out.println("<td>"+ planta.getPrice()+ "</td>");
                    out.println("<td>"+ planta.getId_category()+ "</td>");
                    out.println("</tr>");
                }
                %>
                </tr>
                </colorgroup>
            </table> </center> 
                </fieldset> </center>
        <br>
        <hr>
        <a href="listaCategorias.jsp" ><center><h2>Listas de Plantas por Categoria </h2></center><a/>
        <form name="formularioPlantas" action="ControladorVivero"method="get"><center>
            <fieldset style="width:500px">
            <legend><h2 style="color:black;"><center>Funciones</center></h2> </legend>
            <p style="text-align:left;"> 
            CÓDIGO PLANTA:      <input type="text" name="id" size="10" placeholder="Ingrese Id" align='left' > <br>
            NOMBRE PLANTA:      <input type="text" name="name" size="25"placeholder="Ingrese Nombre"><br>
            DESCRIPCIÓN PLANTA: <input type="text" name="description" size="25"placeholder="Ingrese Descripcion Planta..."><br>
            PRECIO PLANTA:      <input type="text" name="price" size="10"placeholder="Ingrese Precio"><br>
            PATRON ITERATOR >>> <br> 
            CATEGORIA > 
            <select name="category">
                <%
                 CRUDCategorias crudCategorias = new CRUDCategorias();
                 IteratorListaCategorias listaCategorias = new IteratorListaCategorias();
                 LinkedList<Category> listCat = crudCategorias.listarCategorias();
                 
                 for (int x = 0; x < listCat.size();x++){
                 listaCategorias.add(listCat.get(x).getId());
                 }
                 
                 Iterator<Category> iteradorCategorias = listaCategorias.iterator();
                 while(iteradorCategorias.hasNext()){
                     Category categoria = (Category)iteradorCategorias.next();
                     out.println("<option value = \""+categoria.getId()+ "\">" + categoria.getName()+ "</option>");
                 }
                 %>
            </select>
            </p>
            </fieldset>
            <hr>
            <br>
            <input type="submit" name="insertarplanta" value="AÑADIR PLANTA" style="color: green; border: 3px solid; width: 150px; height: 30px">
            <input type="submit" name="modificarplanta" value="ACTUALIZAR PLANTA"style="color: blue; border: 3px solid; width: 170px; height: 30px">
            <input type="submit" name="borrarplanta" value="BORRAR PLANTA" style="color: red; border: 3px solid; width: 160px; height: 30px">
            </center>
        </form>
        <br>
        <hr>
    </body>
</html>